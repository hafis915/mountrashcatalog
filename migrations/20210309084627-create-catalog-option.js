'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('CatalogOptions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Name: {
        type: Sequelize.STRING
      },
      ImageUri: {
        type: Sequelize.STRING
      },
      Description: {
        type: Sequelize.STRING
      },
      Quantifier: {
        type: Sequelize.STRING
      },
      WeightInGram: {
        type: Sequelize.INTEGER
      },
      EditorId: {
        type: Sequelize.STRING
      },
      CatalogItemId: {
        type: Sequelize.INTEGER,
        references :{
          model : 'CatalogItems',
          key : 'id'
        },
        onUpdate : 'cascade',
        onDelete : 'cascade'
      },
      Csr: {
        type: Sequelize.INTEGER
      },
      CatalogLabelId: {
        type: Sequelize.INTEGER,
        references : {
          model : 'CatalogLabels',
          key : 'id'
        },
        onUpdate : 'cascade',
        onDelete : 'cascade'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('CatalogOptions');
  }
};