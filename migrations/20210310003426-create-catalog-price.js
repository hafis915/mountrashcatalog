"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("CatalogPrices", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      PricePerUnit: {
        type: Sequelize.INTEGER,
      },
      CatalogLocationId: {
        type: Sequelize.INTEGER,
        references: {
          model: "CatalogLocations",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      IssuedTime: {
        type: Sequelize.DATEONLY,
      },
      CatalogPriceTypeId: {
        type: Sequelize.INTEGER,
        references: {
          model: "CatalogPriceTypes",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      CatalogLabelId: {
        type: Sequelize.INTEGER,
        references: {
          model: "CatalogLabels",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      CustomTippingFee: {
        type: Sequelize.FLOAT,
      },
      EditorId: {
        type: Sequelize.STRING,
      },
      IsDefaultFee: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("CatalogPrices");
  },
};
