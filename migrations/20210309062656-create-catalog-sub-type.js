'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('CatalogSubTypes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      CatalogTypeId: {
        type: Sequelize.INTEGER,
        references :{
          model : 'CatalogTypes',
          key : 'id'
        },
        onUpdate : 'cascade',
        onDelete : 'cascade'
      },
      Name: {
        type: Sequelize.STRING
      },
      Description: {
        type: Sequelize.STRING
      },
      ImageUri: {
        type: Sequelize.STRING
      },
      DefaultTippingFee: {
        type: Sequelize.FLOAT
      },
      EditorId: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('CatalogSubTypes');
  }
};