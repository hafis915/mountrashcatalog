'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CatalogItem.hasMany(models.CatalogOption)
      // define association here
    }
  };
  CatalogItem.init({
    Name: DataTypes.STRING,
    Description: DataTypes.STRING,
    ImageUri: DataTypes.STRING,
    EditorId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CatalogItem',
  });
  return CatalogItem;
};