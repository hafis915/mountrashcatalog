'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogOption extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CatalogOption.belongsTo(models.CatalogItem)
      CatalogOption.belongsTo(models.CatalogLabel)
      // define association here
    }
  };
  CatalogOption.init({
    Name: DataTypes.STRING,
    ImageUri: DataTypes.STRING,
    Description: DataTypes.STRING,
    Quantifier: DataTypes.STRING,
    WeightInGram: DataTypes.INTEGER,
    EditorId: DataTypes.STRING,
    CatalogItemId: DataTypes.INTEGER,
    Csr: DataTypes.INTEGER,
    CatalogLabelId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'CatalogOption',
  });
  return CatalogOption;
};