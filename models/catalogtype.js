'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CatalogType.hasMany(models.CatalogSubType)
      // define association here
    }
  };
  CatalogType.init({
    Name: DataTypes.STRING,
    Description: DataTypes.STRING,
    ImageUri: DataTypes.STRING,
    EditorId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CatalogType',
  });
  return CatalogType;
};