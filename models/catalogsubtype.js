'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogSubType extends Model {
    static associate(models) {
      CatalogSubType.belongsTo(models.CatalogType)
      CatalogSubType.hasMany(models.CatalogLabel)
    }
  };
  CatalogSubType.init({
    CatalogTypeId: DataTypes.INTEGER,
    Name: DataTypes.STRING,
    Description: DataTypes.STRING,
    ImageUri: DataTypes.STRING,
    DefaultTippingFee: DataTypes.FLOAT,
    EditorId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CatalogSubType',
  });
  return CatalogSubType;
};