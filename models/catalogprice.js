'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogPrice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CatalogPrice.belongsTo(models.CatalogLocation)
      CatalogPrice.belongsTo(models.CatalogPriceType)
      CatalogPrice.belongsTo(models.CatalogLabel)
      // define association here
    }
  };
  CatalogPrice.init({
    PricePerUnit: DataTypes.INTEGER,
    CatalogLocationId: DataTypes.INTEGER,
    IssuedTime: DataTypes.DATEONLY,
    CatalogPriceTypeId: DataTypes.INTEGER,
    CatalogLabelId: DataTypes.INTEGER,
    CustomTippingFee: DataTypes.FLOAT,
    EditorId: DataTypes.STRING,
    IsDefaultFee: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'CatalogPrice',
  });
  return CatalogPrice;
};