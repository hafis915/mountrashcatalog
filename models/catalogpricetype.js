'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogPriceType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CatalogPriceType.hasMany(models.CatalogPrice)
      // define association here
    }
  };
  CatalogPriceType.init({
    Name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CatalogPriceType',
  });
  return CatalogPriceType;
};