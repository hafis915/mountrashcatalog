'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogLocation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CatalogLocation.hasMany(models.CatalogPrice)
      // define association here
    }
  };
  CatalogLocation.init({
    ProvinceName: DataTypes.STRING,
    CountryName: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CatalogLocation',
  });
  return CatalogLocation;
};