'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogLabel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CatalogLabel.belongsTo(models.CatalogSubType)
      CatalogLabel.hasMany(models.CatalogOption)
      CatalogLabel.hasMany(models.CatalogPrice)
      // define association here
    }
  };
  CatalogLabel.init({
    Name: DataTypes.STRING,
    Description: DataTypes.STRING,
    CatalogSubTypeId: DataTypes.INTEGER,
    EditorId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CatalogLabel',
  });
  return CatalogLabel;
};