const route = require("express").Router();
const { catalogSubTypeController } = require("../controllers/index");

route.post("/", catalogSubTypeController.createCatalogSubType);
route.get("/", catalogSubTypeController.readAllCatalogSubType);
route.get("/:id", catalogSubTypeController.readOneCatalogSubType);
route.put("/:id", catalogSubTypeController.updateCatalogSubType);
route.delete("/:id", catalogSubTypeController.deleteCatalogSubType);

module.exports = route;
