const route = require("express").Router();
const { catalogTypeController } = require("../controllers/index");

route.post("/", catalogTypeController.createCatalogType);
route.get("/", catalogTypeController.readAllCatalogType);
route.get("/:id", catalogTypeController.readOneCatalogType);
route.put("/:id", catalogTypeController.updateCatalogType);
route.delete("/:id", catalogTypeController.deleteCatalogType);

module.exports = route;
