const route = require("express").Router();
const { catalogLocationController } = require("../controllers/index");

route.post("/", catalogLocationController.createCatalogLocation);
route.get("/", catalogLocationController.readAllCatalogLocation);
route.get("/:id", catalogLocationController.readOneCatalogLocation);
route.put("/:id", catalogLocationController.updateCatalogLocation);
route.delete("/:id", catalogLocationController.deleteCatalogLocation);

module.exports = route;