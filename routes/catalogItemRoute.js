const route = require("express").Router();
const { catalogItemController } = require("../controllers/index");

route.post("/", catalogItemController.createCatalogItem);
route.get("/", catalogItemController.readAllCatalogItem);
route.get("/:id", catalogItemController.readOneCatalogItem);
route.put("/:id", catalogItemController.updateCatalogItem);
route.delete("/:id", catalogItemController.deleteCatalogItem);

module.exports = route;
