const route = require("express").Router();
const { catalogPriceController } = require("../controllers/index");

route.post("/", catalogPriceController.createCatalogPrice);
route.get("/", catalogPriceController.readAllCatalogPrice);
route.get("/:id", catalogPriceController.readOneCatalogPrice);
route.put("/:id", catalogPriceController.updateCatalogPrice);
route.delete("/:id", catalogPriceController.deleteCatalogPrice);

module.exports = route;
