const route = require("express").Router();
const { catalogPriceTypeController } = require("../controllers/index");

route.post("/", catalogPriceTypeController.createCatalogPriceType);
route.get("/", catalogPriceTypeController.readAllCatalogPriceType);
route.get("/:id", catalogPriceTypeController.readOneCatalogPriceType);
route.put("/:id", catalogPriceTypeController.updateCatalogPriceType);
route.delete("/:id", catalogPriceTypeController.deleteCatalogPriceType);

module.exports = route;