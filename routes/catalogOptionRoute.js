const route = require("express").Router();
const { catalogOptionController } = require("../controllers/index");

route.post("/", catalogOptionController.createCatalogOption);
route.get("/", catalogOptionController.readAllCatalogOption);
route.get("/:id", catalogOptionController.readOneCatalogOption);
route.put("/:id", catalogOptionController.updateCatalogOption);
route.delete("/:id", catalogOptionController.deleteCatalogOption);

module.exports = route;
