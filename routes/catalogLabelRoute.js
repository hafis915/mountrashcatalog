const route = require("express").Router();
const { catalogLabelController } = require("../controllers/index");

route.post("/", catalogLabelController.createCatalogLabel);
route.get("/", catalogLabelController.readAllCatalogLabel);
route.get("/:id", catalogLabelController.readOneCatalogLabel);
route.put("/:id", catalogLabelController.updateCatalogLabel);
route.delete("/:id", catalogLabelController.deleteCatalogLabel);
module.exports = route;
