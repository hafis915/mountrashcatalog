const route = require("express").Router();
const catalogItemRoute = require("./catalogItemRoute");
const catalogTypeRoute = require("./catalogTypeRoute");
const catalogSubTypeRoute = require("./catalogSubTypeRoute");
const catalogLabelRoute = require("./catalogLabelRoute");
const catalogOptionRoute = require("./catalogOptionRoute");
const catalogLocationRoute = require("./catalogLocationRoute");
const catalogPriceTypeRoute = require("./catalogPriceTypeRoute");
const catalogPriceRoute = require("./catalogPriceRoute");

route.use("/catalogitem", catalogItemRoute);
route.use("/catalogtype", catalogTypeRoute);
route.use("/catalogsubtype", catalogSubTypeRoute);
route.use("/cataloglabel", catalogLabelRoute);
route.use("/catalogoption", catalogOptionRoute);
route.use("/cataloglocation", catalogLocationRoute);
route.use("/catalogpricetype", catalogPriceTypeRoute);
route.use("/catalogprice", catalogPriceRoute);

module.exports = route;
