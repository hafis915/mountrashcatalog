const { CatalogSubType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const {CatalogTypeId , Name , Description, ImageUri, DefaultTippingFee, EditorId } = req.body;
    const newCatalogSubType = { CatalogTypeId , Name , Description, ImageUri, DefaultTippingFee, EditorId };
    const addNewCatalogSubType = await CatalogSubType.create(newCatalogSubType);
    res.status(201).json(addNewCatalogSubType);
  } catch (error) {
    console.log(error);
  }
};
