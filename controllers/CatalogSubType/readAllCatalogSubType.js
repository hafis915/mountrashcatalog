const { CatalogSubType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogSubType = await CatalogSubType.findAll({include : ['CatalogType']});
    res.status(200).json(getCatalogSubType);
  } catch (error) {
    console.log(error);
  }
};
