const { CatalogSubType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { CatalogTypeId , Name , Description, ImageUri, DefaultTippingFee, EditorId } = req.body;
    const updatedDataCatalogSubType = { CatalogTypeId , Name , Description, ImageUri, DefaultTippingFee, EditorId };
    const updatedCatalogSubType = await CatalogSubType.update(
      updatedDataCatalogSubType,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogSubType[1][0]);
  } catch (error) {
    console.log(error);
  }
};
