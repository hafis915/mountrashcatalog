const {CatalogSubType} = require('../../models')

module.exports = async (req,res) => {
    try {
        const {id} = req.params
        const getOneCatalogSubType = await CatalogSubType.findOne({
            where : {
                id 
            },
            include : ['CatalogType']
        })
        res.status(200).json(getOneCatalogSubType)
    } catch (error) {
        console.log(error)
    }
}