module.exports = {
  createCatalogSubType: require("./createCatalogSubType"),
  readAllCatalogSubType: require("./readAllCatalogSubType"),
  readOneCatalogSubType: require("./readOneCatalogSubType"),
  updateCatalogSubType: require("./updateCatalogSubType"),
  deleteCatalogSubType: require("./deleteCatalogSubType"),
};
