const { CatalogSubType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCatalogSubType = await CatalogSubType.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ msg: "success delete data" });
  } catch (error) {
    console.log(error);
  }
};
