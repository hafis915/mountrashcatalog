const {
  createCatalogOption,
  readAllCatalogOption,
  readOneCatalogOption,
  updateCatalogOption,
  deleteCatalogOption,
} = require("./CatalogOptions/index");

module.exports = {
  createCatalogOption,
  readAllCatalogOption,
  readOneCatalogOption,
  updateCatalogOption,
  deleteCatalogOption,
};
