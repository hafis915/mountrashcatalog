const {
  readAllCatalogLocation,
  readOneCatalogLocation,
  createCatalogLocation,
  updateCatalogLocation,
  deleteCatalogLocation,
} = require("./CatalogLocation/index");

module.exports = {
  readAllCatalogLocation,
  readOneCatalogLocation,
  createCatalogLocation,
  updateCatalogLocation,
  deleteCatalogLocation,
};
