const catalogItemController = require("./catalogItems");
const catalogTypeController = require("./catalogType");
const catalogSubTypeController = require("./catalogSubType");
const catalogLabelController = require("./catalogLabel");
const catalogOptionController = require("./catalogOption");
const catalogLocationController = require("./catalogLocation");
const catalogPriceTypeController = require("./catalogPriceType");
const catalogPriceController = require("./catalogPrice");

module.exports = {
  catalogItemController,
  catalogTypeController,
  catalogSubTypeController,
  catalogLabelController,
  catalogOptionController,
  catalogLocationController,
  catalogPriceTypeController,
  catalogPriceController,
};
