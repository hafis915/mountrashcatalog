module.exports = {
    createCatalogPrice: require("./createCatalogPrice"),
    readAllCatalogPrice: require("./readAllCatalogPrice"),
    readOneCatalogPrice: require("./readOneCatalogPrice"),
    updateCatalogPrice: require("./updateCatalogPrice"),
    deleteCatalogPrice: require("./deleteCatalogPrice"),
  };
  