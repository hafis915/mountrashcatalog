const { CatalogPrice } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCatalogPrice = await CatalogPrice.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ msg: "success delete price data" });
  } catch (error) {
    console.log(error);
  }
};