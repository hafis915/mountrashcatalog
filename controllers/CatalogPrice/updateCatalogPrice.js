const { CatalogPrice } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const {
      PricePerUnit,
      CatalogLocationId,
      IssuedTime,
      CatalogPriceTypeId,
      CatalogLabelId,
      CustomTippingFee,
      EditorId,
      IsDefaultFee,
    } = req.body;
    const updatedDataCatalogPrice = {
      PricePerUnit,
      CatalogLocationId,
      IssuedTime,
      CatalogPriceTypeId,
      CatalogLabelId,
      CustomTippingFee,
      EditorId,
      IsDefaultFee,
    };
    const updatedCatalogPrice = await CatalogPrice.update(
      updatedDataCatalogPrice,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogPrice[1][0]);
  } catch (error) {
    console.log(error);
  }
};
