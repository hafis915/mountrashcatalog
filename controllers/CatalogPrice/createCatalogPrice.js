const { CatalogPrice } = require("../../models");

module.exports = async (req, res) => {
  try {
    const {
      PricePerUnit,
      CatalogLocationId,
      IssuedTime,
      CatalogPriceTypeId,
      CatalogLabelId,
      CustomTippingFee,
      EditorId,
      IsDefaultFee,
    } = req.body;
    const newCatalogPrice = {
      PricePerUnit,
      CatalogLocationId,
      IssuedTime,
      CatalogPriceTypeId,
      CatalogLabelId,
      CustomTippingFee,
      EditorId,
      IsDefaultFee,
    };
    const addNewCatalogPrice = await CatalogPrice.create(newCatalogPrice);
    res.status(201).json(addNewCatalogPrice);
  } catch (error) {
    console.log(error);
  }
};
