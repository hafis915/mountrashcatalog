const {CatalogPrice} = require('../../models')

module.exports = async (req,res) => {
    try {
        const {id} = req.params
        const getOneCatalogPrice = await CatalogPrice.findOne({
            where : {
                id 
            },
            include : ['CatalogPriceType', 'CatalogLocation', 'CatalogLabel']
        })
        res.status(200).json(getOneCatalogPrice)
    } catch (error) {
        console.log(error)
    }
}