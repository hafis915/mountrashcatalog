const { CatalogPrice } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogPrice = await CatalogPrice.findAll({
      include: ["CatalogLocation", "CatalogPriceType", "CatalogLabel"],
    });
    res.status(200).json(getCatalogPrice);
  } catch (error) {
    console.log(error);
  }
};
