const {
  createCatalogPriceType,
  readAllCatalogPriceType,
  readOneCatalogPriceType,
  updateCatalogPriceType,
  deleteCatalogPriceType,
} = require("./CatalogPriceType/index");

module.exports = {
  createCatalogPriceType,
  readAllCatalogPriceType,
  readOneCatalogPriceType,
  updateCatalogPriceType,
  deleteCatalogPriceType,
};
