const { CatalogItem } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { Name, Description, ImageUri, EditorId } = req.body;
    const updatedDataCatalogItem = { Name, Description, ImageUri, EditorId };
    const updatedCatalogItem = await CatalogItem.update(
      updatedDataCatalogItem,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogItem[1][0]);
  } catch (error) {
    console.log(error);
  }
};
