const { CatalogItem } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogItem = await CatalogItem.findAll();
    res.status(200).json(getCatalogItem);
  } catch (error) {
    console.log(error);
  }
};
