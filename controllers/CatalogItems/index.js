module.exports = {
  createCatalogItem: require("./createCatalogItem"),
  readAllCatalogItem: require("./readAllCatalogItem"),
  readOneCatalogItem: require("./readOneCatalogItem"),
  updateCatalogItem: require("./updateCatalogItem"),
  deleteCatalogItem: require("./deleteCatalogItem"),
};
