const {CatalogItem} = require('../../models')

module.exports = async (req,res) => {
    try {
        const {id} = req.params
        const getOneCatalogItem = await CatalogItem.findOne({
            where : {
                id 
            }
        })
        res.status(200).json(getOneCatalogItem)
    } catch (error) {
        console.log(error)
    }
}