const { CatalogItem } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { Name, Description, ImageUri, EditorId } = req.body;
    const newCatalogItem = { Name, Description, ImageUri, EditorId };
    const addNewCatalogItem = await CatalogItem.create(newCatalogItem);
    res.status(201).json(addNewCatalogItem);
  } catch (error) {
    console.log(error);
  }
};
