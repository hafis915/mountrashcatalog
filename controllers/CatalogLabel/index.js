module.exports = {
    createCatalogLabel: require("./createCatalogLabel"),
    readAllCatalogLabel: require("./readAllCatalogLabel"),
    readOneCatalogLabel: require("./readOneCatalogLabel"),
    updateCatalogLabel: require("./updateCatalogLabel"),
    deleteCatalogLabel: require("./deleteCatalogLabel"),
  };