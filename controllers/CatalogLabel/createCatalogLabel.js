const { CatalogLabel } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { CatalogSubTypeId, Name, Description, EditorId } = req.body;
    const newCatalogLabel = { CatalogSubTypeId, Name, Description, EditorId };
    const addNewCatalogLabel = await CatalogLabel.create(newCatalogLabel);
    res.status(201).json(addNewCatalogLabel);
  } catch (error) {
    console.log(error);
  }
};
