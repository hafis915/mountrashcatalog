const { CatalogLabel } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { CatalogSubTypeId, Name, Description, EditorId } = req.body;
    const updatedDataCatalogLabel = { CatalogSubTypeId, Name, Description, EditorId };
    const updatedCatalogLabel = await CatalogLabel.update(
      updatedDataCatalogLabel,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogLabel[1][0]);
  } catch (error) {
    console.log(error);
  }
};
