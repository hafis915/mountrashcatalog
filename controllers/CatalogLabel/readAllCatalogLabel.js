const { CatalogLabel } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogLabel = await CatalogLabel.findAll({include : ['CatalogSubType']});
    res.status(200).json(getCatalogLabel);
  } catch (error) {
    console.log(error);
  }
};
