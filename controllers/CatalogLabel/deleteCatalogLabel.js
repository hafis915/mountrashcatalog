const { CatalogLabel } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCatalogLabel = await CatalogLabel.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ msg: "success delete Catalog Label data" });
  } catch (error) {
    console.log(error);
  }
};
