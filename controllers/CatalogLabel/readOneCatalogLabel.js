const {CatalogLabel} = require('../../models')

module.exports = async (req,res) => {
    try {
        const {id} = req.params
        const getOneCatalogLabel = await CatalogLabel.findOne({
            where : {
                id 
            },
            include : ['CatalogSubType']
        })
        res.status(200).json(getOneCatalogLabel)
    } catch (error) {
        console.log(error)
    }
}