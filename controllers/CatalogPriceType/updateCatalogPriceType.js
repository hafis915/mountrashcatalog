const { CatalogPriceType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { Name } = req.body;
    const updatedDataCatalogPriceType = { Name };
    const updatedCatalogPriceType = await CatalogPriceType.update(
      updatedDataCatalogPriceType,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogPriceType[1][0]);
  } catch (error) {
    console.log(error);
  }
};
