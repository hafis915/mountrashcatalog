module.exports = {
    createCatalogPriceType: require("./createCatalogPriceType"),
    readAllCatalogPriceType: require("./readAllCatalogPriceType"),
    readOneCatalogPriceType: require("./readOneCatalogPriceType"),
    updateCatalogPriceType: require("./updateCatalogPriceType"),
    deleteCatalogPriceType: require("./deleteCatalogPriceType"),
  };
  