const { CatalogPriceType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const getOneCatalogPriceType = await CatalogPriceType.findOne({
      where: {
        id,
      },
    });
    res.status(200).json(getOneCatalogPriceType);
  } catch (error) {
    console.log(error);
  }
};
