const { CatalogPriceType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCatalogPriceType = await CatalogPriceType.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ msg: "success delete catalog price type data" });
  } catch (error) {
    console.log(error);
  }
};
