const { CatalogPriceType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogPriceType = await CatalogPriceType.findAll();
    res.status(200).json(getCatalogPriceType);
  } catch (error) {
    console.log(error);
  }
};
