const { CatalogPriceType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { Name } = req.body;
    const newCatalogPriceType = { Name };
    const addNewCatalogPriceType = await CatalogPriceType.create(
      newCatalogPriceType
    );
    res.status(201).json(addNewCatalogPriceType);
  } catch (error) {
    console.log(error);
  }
};
