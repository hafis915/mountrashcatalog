const {
  createCatalogLabel,
  readAllCatalogLabel,
  readOneCatalogLabel,
  updateCatalogLabel,
  deleteCatalogLabel,
} = require("./CatalogLabel/index");

module.exports = {
  createCatalogLabel,
  readAllCatalogLabel,
  readOneCatalogLabel,
  updateCatalogLabel,
  deleteCatalogLabel,
};
