const {
  createCatalogItem,
  readAllCatalogItem,
  readOneCatalogItem,
  updateCatalogItem,
  deleteCatalogItem,
} = require("./CatalogItems/index");

module.exports = {
  createCatalogItem,
  readAllCatalogItem,
  readOneCatalogItem,
  updateCatalogItem,
  deleteCatalogItem,
};
