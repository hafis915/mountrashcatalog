module.exports = {
    createCatalogOption: require("./createCatalogOption"),
    readAllCatalogOption: require("./readAllCatalogOption"),
    readOneCatalogOption: require("./readOneCatalogOption"),
    updateCatalogOption: require("./updateCatalogOption"),
    deleteCatalogOption: require("./deleteCatalogOption"),
  };