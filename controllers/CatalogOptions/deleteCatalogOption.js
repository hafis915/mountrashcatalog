const { CatalogOption } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCatalogOption = await CatalogOption.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ msg: "success delete option data" });
  } catch (error) {
    console.log(error);
  }
};
