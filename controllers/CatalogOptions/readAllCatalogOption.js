const { CatalogOption } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogOption = await CatalogOption.findAll({include : ['CatalogItem', 'CatalogLabel']});
    res.status(200).json(getCatalogOption);
  } catch (error) {
    console.log(error);
  }
};
