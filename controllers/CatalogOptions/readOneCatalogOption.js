const {CatalogOption} = require('../../models')

module.exports = async (req,res) => {
    try {
        const {id} = req.params
        const getOneCatalogOption = await CatalogOption.findOne({
            where : {
                id 
            },
            include : ['CatalogItem', 'CatalogLabel']
        })
        res.status(200).json(getOneCatalogOption)
    } catch (error) {
        console.log(error)
    }
}