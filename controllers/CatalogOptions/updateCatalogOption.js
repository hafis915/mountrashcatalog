const { CatalogOption } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { Name , ImageUri , Description, Quantifier, WeightInGram, EditorId, CatalogItemId, CatalogLabelId, Csr } = req.body;
    const updatedDataCatalogOption = { Name , ImageUri , Description, Quantifier, WeightInGram, EditorId, CatalogItemId, CatalogLabelId, Csr };
    const updatedCatalogOption = await CatalogOption.update(
      updatedDataCatalogOption,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogOption[1][0]);
  } catch (error) {
    console.log(error);
  }
};
