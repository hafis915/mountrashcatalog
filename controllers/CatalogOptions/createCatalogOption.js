const { CatalogOption } = require("../../models");

module.exports = async (req, res) => {
  try {
    const {Name , ImageUri , Description, Quantifier, WeightInGram, EditorId, CatalogItemId, CatalogLabelId, Csr } = req.body;
    const newCatalogOption = { Name , ImageUri , Description, Quantifier, WeightInGram, EditorId, CatalogItemId, CatalogLabelId, Csr };
    const addNewCatalogOption = await CatalogOption.create(newCatalogOption);
    res.status(201).json(addNewCatalogOption);
  } catch (error) {
    console.log(error);
  }
};
