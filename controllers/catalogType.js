const {
  createCatalogType,
  deleteCatalogType,
  readAllCatalogType,
  readOneCatalogType,
  updateCatalogType,
} = require("./CatalogType/index");

module.exports = {
  createCatalogType,
  deleteCatalogType,
  readAllCatalogType,
  readOneCatalogType,
  updateCatalogType,
};
