const { CatalogType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { Name, Description, ImageUri, EditorId } = req.body;
    const newCatalogType = { Name, Description, ImageUri, EditorId };
    const addNewCatalogType = await CatalogType.create(newCatalogType);
    res.status(201).json(addNewCatalogType);
  } catch (error) {
    console.log(error);
  }
};
