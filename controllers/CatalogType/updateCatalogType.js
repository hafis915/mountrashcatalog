const { CatalogType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { Name, Description, ImageUri, EditorId } = req.body;
    const updatedDataCatalogType = { Name, Description, ImageUri, EditorId };
    const updatedCatalogType = await CatalogType.update(
      updatedDataCatalogType,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogType[1][0]);
  } catch (error) {
    console.log(error);
  }
};
