const { CatalogType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCatalogType = await CatalogType.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ msg: "success delete data" });
  } catch (error) {
    console.log(error);
  }
};
