module.exports = {
    createCatalogType: require("./createCatalogType"),
    readAllCatalogType: require("./readAllCatalogType"),
    readOneCatalogType: require("./readOneCatalogType"),
    updateCatalogType: require("./updateCatalogType"),
    deleteCatalogType: require("./deleteCatalogType"),
  };
  