const {CatalogType} = require('../../models')

module.exports = async (req,res) => {
    try {
        const {id} = req.params
        const getOneCatalogType = await CatalogType.findOne({
            where : {
                id 
            }
        })
        res.status(200).json(getOneCatalogType)
    } catch (error) {
        console.log(error)
    }
}