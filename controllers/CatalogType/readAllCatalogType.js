const { CatalogType } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogType = await CatalogType.findAll();
    res.status(200).json(getCatalogType);
  } catch (error) {
    console.log(error);
  }
};
