const { CatalogLocation } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { ProvinceName, CountryName } = req.body;
    const updatedDataCatalogLocation = { ProvinceName, CountryName };
    const updatedCatalogLocation = await CatalogLocation.update(
      updatedDataCatalogLocation,
      {
        where: {
          id,
        },
        returning: true,
      }
    );
    res.status(201).json(updatedCatalogLocation[1][0]);
  } catch (error) {
    console.log(error);
  }
};
