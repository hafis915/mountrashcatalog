const { CatalogLocation } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { ProvinceName, CountryName } = req.body;
    const newCatalogLocation = { ProvinceName, CountryName };
    const addNewCatalogLocation = await CatalogLocation.create(
      newCatalogLocation
    );
    res.status(201).json(addNewCatalogLocation);
  } catch (error) {
    console.log(error);
  }
};
