module.exports = {
  createCatalogLocation: require("./createCatalogLocation"),
  readAllCatalogLocation: require("./readAllCatalogLocation"),
  readOneCatalogLocation: require("./readOneCatalogLocation"),
  updateCatalogLocation: require("./updateCatalogLocation"),
  deleteCatalogLocation: require("./deleteCatalogLocation"),
};
