const { CatalogLocation } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCatalogLocation = await CatalogLocation.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ msg: "success delete Location data" });
  } catch (error) {
    console.log(error);
  }
};
