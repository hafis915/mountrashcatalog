const { CatalogLocation } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const getOneCatalogLocation = await CatalogLocation.findOne({
      where: {
        id,
      },
    });
    res.status(200).json(getOneCatalogLocation);
  } catch (error) {
    console.log(error);
  }
};
