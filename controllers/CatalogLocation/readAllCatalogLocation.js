const { CatalogLocation } = require("../../models");

module.exports = async (req, res) => {
  try {
    const getCatalogLocation = await CatalogLocation.findAll();
    res.status(200).json(getCatalogLocation);
  } catch (error) {
    console.log(error);
  }
};
