const {
  createCatalogPrice,
  readAllCatalogPrice,
  readOneCatalogPrice,
  updateCatalogPrice,
  deleteCatalogPrice,
} = require("./CatalogPrice/index");

module.exports = {
  createCatalogPrice,
  readAllCatalogPrice,
  readOneCatalogPrice,
  updateCatalogPrice,
  deleteCatalogPrice,
};
