const {
  createCatalogSubType,
  readAllCatalogSubType,
  readOneCatalogSubType,
  updateCatalogSubType,
  deleteCatalogSubType,
} = require("./CatalogSubType/index");

module.exports = {
  createCatalogSubType,
  readAllCatalogSubType,
  readOneCatalogSubType,
  updateCatalogSubType,
  deleteCatalogSubType,
};
